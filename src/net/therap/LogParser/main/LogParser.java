package net.therap.LogParser.main;

import net.therap.LogParser.model.Log;
import net.therap.LogParser.service.InputService;
import net.therap.LogParser.service.LogService;
import net.therap.LogParser.service.PrintService;

import java.util.*;

/**
 * @author anwar
 * @since 1/3/18
 */
public class LogParser {
    public static void main(String[] args) {
        try {
            InputService inputService = new InputService();
            LogService logService = new LogService();
            PrintService printService = new PrintService();
            Scanner sc = inputService.getInputFileReader();
            int[] totalTime = new int[24];
            int[] getCounts = new int[24];
            int[] postCounts = new int[24];
            Map<Integer, Set<String>> uniqueURIs = new HashMap<>();
            String[] time = logService.assignTimesByAmPmFormat();
            while (sc.hasNextLine()) {
                String line = sc.nextLine();
                String[] splitAllWords = line.split(" ");
                logService.increaseGetPostUriTimeCounts(splitAllWords, getCounts, postCounts, uniqueURIs, totalTime);
            }
            List<Log> logList = printService.insertLogsAndSort(time, uniqueURIs, getCounts, postCounts, totalTime);
            printService.print(logList);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
