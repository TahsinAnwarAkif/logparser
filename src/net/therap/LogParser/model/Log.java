package net.therap.LogParser.model;

import java.io.Serializable;

/**
 * @author anwar
 * @since 1/3/18
 */
public class Log implements Comparable<Log>, Serializable {

    private static final Long serialVersionUID = 1L;
    private int hour;
    private String time;
    private int uniqueURIs;
    private int getCounts;
    private int postCounts;
    private int reponseTime;

    public Log(int hour, String time, int uniqueURIs, int getCounts, int postCounts, int reponseTime) {
        this.hour = hour;
        this.time = time;
        this.uniqueURIs = uniqueURIs;
        this.getCounts = getCounts;
        this.postCounts = postCounts;
        this.reponseTime = reponseTime;
    }

    public int compareTo(Log log){
        return (log.getCounts + log.postCounts)  - (this.getCounts + this.postCounts) ;
    }

    public int getHour() {
        return hour;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getUniqueURIs() {
        return uniqueURIs;
    }

    public void setUniqueURIs(int uniqueURIs) {
        this.uniqueURIs = uniqueURIs;
    }

    public int getGetCounts() {
        return getCounts;
    }

    public void setGetCounts(int getCounts) {
        this.getCounts = getCounts;
    }

    public int getPostCounts() {
        return postCounts;
    }

    public void setPostCounts(int postCounts) {
        this.postCounts = postCounts;
    }

    public int getReponseTime() {
        return reponseTime;
    }

    public void setReponseTime(int reponseTime) {
        this.reponseTime = reponseTime;
    }

    @Override
    public String toString() {
        return
              '\n'+  time  + "           "
               + uniqueURIs + "           "
                 + getCounts + "           "
               + postCounts + "           "
               + reponseTime + "ms         " ;
    }
}
