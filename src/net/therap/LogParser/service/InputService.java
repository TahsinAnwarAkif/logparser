package net.therap.LogParser.service;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

/**
 * @author anwar
 * @since 1/7/18
 */
public class InputService {

    /**
     * prepare scanner for the input file
     */
    public Scanner getInputFileReader() throws FileNotFoundException {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Please Enter a Valid File Name With Extension: ");
        String fileName = scanner.nextLine();
        File file = new File(fileName);
        Scanner fileScanner = new Scanner(file);
        return fileScanner;
    }
}
