package net.therap.LogParser.service;

import net.therap.LogParser.utils.Constants;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

/**
 * @author anwar
 * @since 1/3/18
 */
public class LogService {

    /**
     * assign 24 hour format to AM/PM
     */
    public String[] assignTimesByAmPmFormat() {
        String[] time = new String[24];
        for (int hour = 0; hour < 24; hour++) {
            if (hour == 0) {
                time[hour] = (hour + 12) + "AM-" + (hour + 1) + "AM";
            } else if (hour >= 1 && hour <= 11) {
                String amOrPm = (hour != 11) ? "AM" : "PM";
                time[hour] = hour + "AM-" + (hour + 1) + amOrPm;
            } else if (hour == 12) {
                time[hour] = hour + "PM-" + (hour - 11) + "PM";
            } else {
                time[hour] = (hour - 12) + "PM-" + (hour - 11) + "PM";
            }
        }
        return time;
    }

    /**
     * increment get count for each line
     *
     * @param splitAllWords
     * @param getCounts
     * @return
     */
    public int[] increaseGetCounts(String[] splitAllWords, int[] getCounts) {
        for (String part : splitAllWords) {
            if (part.contains(Constants.GET)) {
                getCounts[Integer.parseInt(splitAllWords[1].substring(0, 2))] += 1;
                break;
            }
        }
        return getCounts;
    }

    /**
     * increment post count for each line
     *
     * @param splitAllWords
     * @param postCounts
     * @return
     */
    public int[] increasePostCounts(String[] splitAllWords, int[] postCounts) {
        for (String part : splitAllWords) {
            if (part.contains(Constants.POST)) {
                postCounts[Integer.parseInt(splitAllWords[1].substring(0, 2))] += 1;
                break;
            }
        }
        return postCounts;
    }

    /**
     * increment total response time for each line
     *
     * @param splitAllWords
     * @param responseTimes
     * @return
     */
    public int[] increaseResponseTimes(String[] splitAllWords, int[] responseTimes) {
        for (String part : splitAllWords) {
            if (part.contains(Constants.TIME_PREFIX)) {
                int hour = Integer.parseInt(splitAllWords[1].substring(0, 2));
                responseTimes[hour] += (part.contains(Constants.TIME_UNIT))
                        ? Integer.parseInt(part.substring(Constants.TIME_PREFIX.length() + 1, part.length() - 2))
                        : Integer.parseInt(part.substring(Constants.TIME_PREFIX.length() + 1, part.length()));
            }
        }
        return responseTimes;
    }

    /**
     * increment unique uri count for each line
     *
     * @param splitAllWords
     * @param uniqueURIs
     * @return
     */
    public Map<Integer, Set<String>> increaseUniqueURIs(String[] splitAllWords, Map<Integer, Set<String>> uniqueURIs) {
        for (String part : splitAllWords) {
            if (part.contains(Constants.URI)) {
                Set<String> uniqueUrisForEachHour = new HashSet<>();
                int hour = Integer.parseInt(splitAllWords[1].substring(0, 2));

                if (uniqueURIs.containsKey(hour)) {
                    uniqueUrisForEachHour = uniqueURIs.get(hour);
                    uniqueUrisForEachHour.add(part);
                }
                uniqueURIs.put(hour, uniqueUrisForEachHour);
                break;
            }
        }
        return uniqueURIs;
    }

    /**
     * increment get, post,time and unique uri for each line
     * calls some LogService methods
     *
     * @param splitAllWords
     * @param getCounts
     * @param postCounts
     * @param uniqueURIs
     * @param totalTime
     * @return
     */
    public void increaseGetPostUriTimeCounts(String[] splitAllWords, int[] getCounts, int[] postCounts,
                                             Map<Integer, Set<String>> uniqueURIs, int[] totalTime) {
        getCounts = increaseGetCounts(splitAllWords, getCounts);
        postCounts = increasePostCounts(splitAllWords, postCounts);
        totalTime = increaseResponseTimes(splitAllWords, totalTime);
        uniqueURIs = increaseUniqueURIs(splitAllWords, uniqueURIs);
    }
}
