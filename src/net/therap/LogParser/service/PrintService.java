package net.therap.LogParser.service;

import net.therap.LogParser.model.Log;

import java.util.*;

/**
 * @author anwar
 * @since 1/7/18
 */
public class PrintService {

    /**
     * print constant strings before printing the list
     */
    public void printColumns() {
        System.out.format("%10s  %10s  %10s  %10s  %10s",
                "Time",
                "Unique URIs",
                "Get Counts",
                "Post Counts",
                "Response Time");
        System.out.println();
    }

    /**
     * populates the counters' list
     * sorts them (descending) according to total G/P counts
     *
     * @param time
     * @param uniqueURIs
     * @param getCounts
     * @param postCounts
     * @param reponseTime
     * @return
     */
    public List<Log> insertLogsAndSort(String[] time, Map<Integer, Set<String>> uniqueURIs,
                                       int[] getCounts, int[] postCounts, int[] reponseTime) {
        List<Log> logList = new ArrayList<>();
        for (int hour = 0; hour < 24; hour++) {
            logList.add(new Log(hour, time[hour],
                    (uniqueURIs.get(hour) != null) ? uniqueURIs.get(hour).size() : 0,
                    getCounts[hour],
                    postCounts[hour],
                    reponseTime[hour]));
        }
        Collections.sort(logList);
        return Collections.unmodifiableList(logList); //todo: unmodifiable
    }

    public void print(List<Log> logList) {
        printColumns();
        for (Log log : logList) {
            System.out.format("%10s%10s%10s%10s%20s",
                    log.getTime(),
                    log.getUniqueURIs(),
                    log.getGetCounts(),
                    log.getPostCounts(),
                    log.getReponseTime());
            System.out.println();
        }
    }
}
