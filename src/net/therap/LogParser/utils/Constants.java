package net.therap.LogParser.utils;

/**
 * @author anwar
 * @since 1/3/18
 */
public class Constants {

    public static final String TIME_PREFIX = "time";
    public static final String TIME_UNIT = "ms";
    public static final String URI = "URI";
    public static final String GET = "G,";
    public static final String POST = "P,";
}
